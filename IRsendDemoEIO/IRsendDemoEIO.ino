/*
  TEST for sending information with EIO Protocol (pin 3)
 */

#include <IRremote.h>

IRsend irsend;

unsigned long id, packet_id, data;
int packet_delay;

void setup()
{
  // Fill ID variables
  id = 0;
  packet_id = 0;
  packet_delay = (id+1)*200;  
}

void loop() {
  // Update packet_id and data values
  packet_id = (packet_id+1)%16;
  data = 40;

  // Send data using the custom EIO protocol
  irsend.sendEIO(id, packet_id, data);

  // Delay between repetitions
  delay(packet_delay);

  // Send repetition using the custom EIO protocol
  irsend.sendEIO(id, packet_id, data);

  // Delay between loops
  delay(500);
}
