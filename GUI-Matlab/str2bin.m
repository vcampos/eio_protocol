function [ out ] = str2bin( in )
    out=zeros(1,length(in));
    for i=1:length(in)
        out(i)=in(i)-'0';
    end
end

