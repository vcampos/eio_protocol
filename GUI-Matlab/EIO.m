%Codi per mostrar les dades amb protocol RS232
clc;
clc;
clear all;
Rt=1;
Rh=1;
Rp=1;
Rl=1;
tt=1;
th=1;
tp=1;
tl=1;

lengthData=22;
lengthIdPacket=4;
lengthIdSensor=3;
idTemp=1;
idHr=0;
idPressure=2;
idLight=3;
dataBeginning=lengthIdSensor+lengthIdPacket+1;
dataEnd=lengthIdSensor+lengthIdPacket+lengthData;

temp=-inf;
hr=-inf;
pressure=-inf;
light=-inf;

%Create the serial object
serialObject = serial('/dev/cu.usbmodem1421', 'BaudRate', 9600, 'Parity', 'none', 'DataBits', 8);
set(serialObject,'Timeout',1000000000000000000000000000);
fopen(serialObject);
i=1;

while (true)
    %obtain the received packet
    receivedPacket=fscanf(serialObject)
    receivedPacket=str2bin(receivedPacket);
    
    while length(receivedPacket)<34
        receivedPacket=[0 receivedPacket];
    end    
    
%     receivedPacket=[0 0 0 1 1 0 0 0 0 1 0 0 1 0 0 1 1];
%     if mod(i,4)==0 %remainder after division
%         receivedPacket=[0 0 1 1 1 0 0 0 0 1 0 1 0 0 0 1 1];
%     elseif mod(i,5)==0 %remainder after division
%         receivedPacket=[0 1 0 1 1 1 1 0 0 1 0 1 0 0 0 1 1];
%     elseif mod(i,7)==0 %remainder after division
%         receivedPacket=[0 1 1 1 1 1 1 0 1 1 0 1 1 1 0 1 1];
%     end
%     i=i+1;
    
    
    %obtain the received sensor
    idSensor=bi2de(flip(receivedPacket(1:lengthIdSensor)));


    %Update the sensor values
    data=receivedPacket(dataBeginning:dataEnd);
    data=bi2de(flip(data));
    if idSensor==idTemp
        Rt=Rt+1;
        temp(Rt)=data;
    elseif idSensor==idHr
        Rh=Rh+1;
        hr(Rh)=data;
    elseif idSensor==idPressure
        Rp=Rp+1;
        pressure(Rp)=data;
    else
        Rl=Rl+1;
        light(Rl)=data;
    end
    
    
    %DISPLAY THE DATA OF ALL THE SENSORS AT THE SAME TIME
    disp(['Temperature: ', num2str(temp(Rt))]);
    disp(['HR: ', num2str(hr(Rh))]);
    disp(['Pressure: ', num2str(pressure(Rp))]);
    disp(['Light: ', num2str(light(Rl))]);
    disp(' ');
    disp(' ');
    
    
    %SUBPLOT: PLOT THE DATA OF ALL THE SENSORS AT THE SAME TIME
    
    %Set the maximum of samples shown at the same time
    if(Rt-tt)>=100    
        tt=Rt-30;
    end
    if(Rh-th)>=100    
        th=Rh-30;
    end
    if(Rp-tp)>=100    
        tp=Rp-30;
    end
    if(Rl-tl)>=100    
        tl=Rl-30;
    end
    
    
    %temp
    subplot(2,2,1)
    plot(tt:Rt, temp(tt:Rt),'-');
    grid on
    set(gca,'XTick',tt-1:30:Rt) %Set X axis
    set(gca,'YTick',-20:4:50) %Set y axis
    ylim([-20 50]);
    title('Temp vs. Packets');
    xlabel('Packets');
    ylabel('Temp (�C)');
    text(Rt,temp(Rt),[num2str(temp(Rt))]);
    
    %hr
    subplot(2,2,2)
    plot (th:Rh, hr(th:Rh),'-');
    grid on
    set(gca,'XTick',th-1:30:Rh) %Set X axis
    set(gca,'YTick',20:5:90) %Set Y axis
    ylim([20 90]);
    title('HR vs. Packets');
    xlabel('Packets');
    ylabel('HR (%)');
    text(Rh,hr(Rh),[num2str(hr(Rh))]);
    
    %pressure
    subplot(2,2,3)
    plot(tp:Rp, pressure(tp:Rp),'-');
    grid on
    set(gca,'XTick',tp-1:30:Rp) %Set X axis
    set(gca,'YTick',600:50:1100) %Set y axis
    ylim([600 1100]);
    title('Pressure vs. Packets');
    xlabel('Packets');
    ylabel('Pressure (hPa)');
    text(Rp,pressure(Rp),[num2str(pressure(Rp))]);
    
    %light
    subplot(2,2,4)
    loglog(tl:Rl, light(tl:Rl),'-');
    grid on
    set(gca,'XTick',tl-1:30:Rl) %Set X axis
    %set(gca,'YTick',1:200:100000) %Set y axis
    ylim([1 100000]);
    title('Light vs. Packets');
    xlabel('Packets');
    ylabel('Light (lm/m2)');
    text(Rl,light(Rl),[num2str(light(Rl))]);
    
    %Refresh the shown plot every time
    drawnow; 
    
end


% Clean up the serial object
fclose(serialObject);
delete(serialObject);
clear serialObject;


