#include "IRremote.h"
#include "IRremoteInt.h"

//==============================================================================
//                           EEEEE  II   OOOOO
//                           E      II  O     O
//                           EEE    II  O     O
//                           E      II  O     O
//                           EEEEE  II   OOOOO
//==============================================================================

#define EIO_BITS          32
#define EIO_HDR_MARK    9000
#define EIO_HDR_SPACE   4500
#define EIO_BIT_MARK     560
#define EIO_ONE_SPACE   1690
#define EIO_ZERO_SPACE   560
#define EIO_RPT_SPACE   2250
#define EIO_FREQUENCY 40

#define ID_BITS 3
#define PACKET_ID_BITS 4
#define DATA_BITS 22
#define REDUNDANCY_BITS ID_BITS  // we use the complementary of the ID to check the integrity of the packet

//+=============================================================================
#if SEND_EIO
unsigned long buildEIOPacket (unsigned long id, unsigned long packet_id, unsigned long data) {
    return (id << (DATA_BITS + REDUNDANCY_BITS + PACKET_ID_BITS)) + (packet_id << (DATA_BITS + REDUNDANCY_BITS))+ (data << REDUNDANCY_BITS) + (7^id);
}

void IRsend::sendEIO (unsigned long id, unsigned long packet_id, unsigned long data)//, unsigned long data,  int nbits)
{
    // Build packet
    unsigned long packet = buildEIOPacket(id, packet_id, data);
    int nbits = EIO_BITS;
    
    // Set IR carrier frequency
    enableIROut(EIO_FREQUENCY);
    
    // Header
    mark(EIO_HDR_MARK);
    space(EIO_HDR_SPACE);
    
    // Data
    for (unsigned long  mask = 1UL << (nbits - 1);  mask;  mask >>= 1) {
        if (packet & mask) {
            mark(EIO_BIT_MARK);
            space(EIO_ONE_SPACE);
        } else {
            mark(EIO_BIT_MARK);
            space(EIO_ZERO_SPACE);
        }
    }
    
    // Footer
    mark(EIO_BIT_MARK);
    space(0);  // Always end with the LED off
}
#endif

//+=============================================================================
// EIOs have a repeat only 4 items long
//
#if DECODE_EIO
bool  IRrecv::decodeEIO (decode_results *results)
{
    long  data   = 0;  // We decode in to here; Start with nothing
    int   offset = 1;  // Index in to results; Skip first entry!?
    
    // Check header "mark"
    if (!MATCH_MARK(results->rawbuf[offset], EIO_HDR_MARK))  return false ;
    offset++;
    
    // Check for repeat
    if ( (irparams.rawlen == 4)
        && MATCH_SPACE(results->rawbuf[offset  ], EIO_RPT_SPACE)
        && MATCH_MARK (results->rawbuf[offset+1], EIO_BIT_MARK )
        ) {
        results->bits        = 0;
        results->value       = REPEAT;
        results->decode_type = EIO;
        return true;
    }
    
    // Check we have enough data
    if (irparams.rawlen < (2 * EIO_BITS) + 4)  return false ;
    
    // Check header "space"
    if (!MATCH_SPACE(results->rawbuf[offset], EIO_HDR_SPACE))  return false ;
    offset++;
    
    // Build the data
    for (int i = 0;  i < EIO_BITS;  i++) {
        // Check data "mark"
        if (!MATCH_MARK(results->rawbuf[offset], EIO_BIT_MARK))  return false ;
        offset++;
        // Suppend this bit
        if      (MATCH_SPACE(results->rawbuf[offset], EIO_ONE_SPACE ))  data = (data << 1) | 1 ;
        else if (MATCH_SPACE(results->rawbuf[offset], EIO_ZERO_SPACE))  data = (data << 1) | 0 ;
        else                                                            return false ;
        offset++;
    }
    
    // Success
    results->bits        = EIO_BITS;
    results->value       = data;
    results->decode_type = EIO;
    
    return true;
}

unsigned long createMask(int nbits){
    unsigned long mask = 1UL;
    for (int i = 1; i < nbits; i++){
        mask = (mask << 1) + 1;
    }
    return mask;
}

// Returns TRUE if the packet has no errors
bool IRrecv::parseEIOPacket(decode_results results, unsigned long *id, unsigned long *packet_id, unsigned long *data){
    unsigned long packet = results.value;
    unsigned long id_mask = createMask(ID_BITS) << (DATA_BITS + REDUNDANCY_BITS + PACKET_ID_BITS);
    unsigned long packet_id_mask = createMask(PACKET_ID_BITS) << (DATA_BITS + REDUNDANCY_BITS);
    unsigned long data_mask = createMask(DATA_BITS) << REDUNDANCY_BITS;
    unsigned long redundancy_mask = createMask(REDUNDANCY_BITS);
    
    *id = (packet & id_mask) >> PACKET_ID_BITS >> DATA_BITS >> REDUNDANCY_BITS;
    *packet_id = (packet & packet_id_mask) >> DATA_BITS >> REDUNDANCY_BITS;
    *data = (packet & data_mask) >> REDUNDANCY_BITS;
    
    unsigned long redundancy = packet & redundancy_mask;
    
    return (((*id) + redundancy) == 7);
}
#endif
