#include <IRremote.h>

#define RECV_PIN 11  // pin where the receiver is connected
#define TOTAL_DEVICES 8  // ID_BITS = 3
#define TOTAL_PACKETS 16  // PACKET_ID_BITS = 4  

// Variables that will store the received data
IRrecv irrecv(RECV_PIN);
decode_results results;
unsigned long id, packet_id, data;

// Vector that stores the next expected packet for each device
int next_packet[TOTAL_DEVICES];

void setup()
{
  Serial.begin(9600);
  //Serial.begin(9600, SERIAL_7E1);
  irrecv.enableIRIn(); // Start the receiver

  for (int i = 0; i < TOTAL_DEVICES; i++)
    next_packet[i] = 0;
}

void loop() {
  if (irrecv.decode(&results))
  { 
    // Check the integrity of the received packet and parse it
    if (irrecv.parseEIOPacket(results, &id, &packet_id, &data))
    {
      // Check that the received packet is not a replica of a previously processed packet
      if (packet_id >= next_packet[id-1])
      {
        // Update the next expected packet ID
        next_packet[id-1] = next_packet[id-1] % TOTAL_PACKETS;

        // Send data through serial port
        Serial.println(results.value, BIN);
        /*
        Serial.println();
        Serial.println(results.value, HEX);
        Serial.println();
        Serial.println("Id");
        Serial.println(id, HEX);
        Serial.println("Packet id");
        Serial.println(packet_id, HEX);
        Serial.println("Data");
        Serial.println(data, HEX);*/
      }
    }
    // Receive the next value
    irrecv.resume();
  } 
  delay(20);
}
